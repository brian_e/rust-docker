#[deny(warnings)]

use std::thread::sleep;
use std::time::Duration;

fn main() {
    println!("sleeping");
    loop {
        sleep(Duration::from_secs(60));
    }
}
