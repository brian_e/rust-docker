FROM ubuntu

RUN apt-get update && \
    apt-get install -y curl \
                       build-essential \
                       gdb \
                       libssl-dev \
                       lsof \
                       net-tools

RUN curl https://sh.rustup.rs -sSf | bash -s -- -y

ENV CARGO_HOME="/root/.cargo"
ENV PATH="/root/.cargo/bin:${PATH}"
RUN /root/.cargo/bin/rustup default nightly

RUN cargo install rustfmt

COPY sleep /sleep
WORKDIR /sleep
RUN cargo install
CMD ["/root/.cargo/bin/sleep"]

WORKDIR /projects
